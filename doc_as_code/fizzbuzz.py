def fizzbuzz(n: int) -> str:
    """Fizzbuzz.
    'Fizz' if n is divisible by 3,
    'Buzz' if n is divisible by 5,
    otherwise n as a string.

    >>> fizzbuzz(1)
    '1'
    """

    return None
